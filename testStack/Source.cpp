///*
//STACK IMPLEMENTATION FROM 
//<<CRACKING THE CODE INTERVIEW>>
//A VERY SIMPLE VERSION
//
//1  class Stack {
//2    Node top;
//3    Node pop() {
//4    if (top != null) {
//5     Object item = top.data;
//6     top = top.next;
//7     return item;
//8    }
//9     return null;
//10   }
//11   void push(Object item) { 
//12   Node t = new Node(item);
//13   t.next = top;
//14   top = t;
//15   } 
//16 }
//*/
//#include<cstdlib>
//// My Implementation
//
//struct Data{
//	int num;
//	Data():num(0){}
//	Data(const Data &e){
//		num = e.num;
//	}
//};
//struct Node{
//	Data mData;
//	struct Node *prev;
//	struct Node *next;
//	Node():prev(nullptr),next(nullptr){}
//	Node(const Node &e){
//		prev = e.prev;
//		next = e.next;
//		mData = e.mData;
//	}
//};
//
//// link list version
//class Stack_LL{
//	Node *top;
//	int size;
//public:
//	Stack_LL():size(0){
//		top = new Node;
//	}
//	~Stack_LL(){
//	}
//
//	bool Push(Data &e){
//		top->mData = e;
//		if(top->next = new Node){
//			top = top->next;
//			size++;
//			return true;
//		}else
//			return false;
//
//	}
//
//	bool Pop(Data &e){
//		e = top->prev->mData;
//		if(top->prev){
//			top = top->prev;
//			delete top->next;
//			top->next = nullptr;
//			size--;
//			return true;
//		}else{
//			delete top;
//			size--;
//			return true;
//		}
//	}
//
//	bool GetTop(Data &e){
//		if(IsEmpty())
//			return false;
//		e = top->mData;
//		return true;
//	}
//	int GetSize(){
//		if(IsEmpty())
//			return 0;
//		else
//			return size;
//	}
//	bool IsEmpty(){
//		if(size == 0)
//			return true;
//		else
//			return false;
//	}
//	bool Clear(){
//		while(top->prev){
//			top = top->prev;
//			delete top->next;
//		}
//	}
//	bool Traverse(bool (*Visit)(Data)){
//		for(int i =0; i< GetSize(); i++){
//			if(!Visit(base[i]))
//				return false;
//		}
//		return true;
//	}
//};
//// Sequence storage version
//// C ver
//struct Stack_C{
//	Data *top;
//	Data *base;
//	const int INIT_SIZE;
//	const int EXPAND_SIZE;
//	int capacity;
//	Stack_C():INIT_SIZE(100),EXPAND_SIZE(10),capacity(INIT_SIZE){
//		base = (Data*)malloc(INIT_SIZE*sizeof(Data));
//		top = base;
//	}
//	~Stack_C(){
//		if(top){
//			top = nullptr;
//		}
//		if(base){
//			free(base);
//			base = nullptr;
//		}
//	}
//
//	//top is like end in std::vector, point to the next of the last element
//	
//	bool Pop(Data &e){
//		if(top == base)
//			return false;
//		else{
//			e = *(top-1);
//			top--;
//		}
//	}
//	bool Push(Data &e){
//		if((top - base) >= capacity ){
//			// expand
//			/*Data *temp = (Data *)malloc((capacity+EXPAND_SIZE)*sizeof(Data));
//			if(!temp)
//				return false;
//			for(int i = 0; i<capacity; i++){
//				*(temp+i) = *(base+i);
//			}
//			base = temp;*/
//			base = (Data *)realloc(base,(capacity+EXPAND_SIZE)*sizeof(Data));
//			if(!base)
//				return false;
//		}
//		top = base+capacity;
//		*top = e;
//		top++;
//		capacity += EXPAND_SIZE;
//		return true;
//	}
//	bool GetTop(Data &e){
//		if(top == base)
//			return false;
//		e = *(top-1);
//		return true;
//	}
//	int GetSize(){
//		return top - base;
//	}
//	bool IsEmpty(){
//		if(top == base)
//			return true;
//		else
//			return false;
//
//	}
//	bool Clear(){
//		top = base;
//		return true;
//	}
//	bool Traverse(bool (*Visit)(Data)){
//		for(int i =0; i< top - base; i++){
//			if(!Visit(*(base+i)))
//				return false;
//		}
//		return true;
//	}
//};
//// C++ ver
//class Stack{
//	Data *top;
//	Data *base;
//	const int INIT_SIZE;
//	const int EXPAND_SIZE;
//	int capacity;
//public:
//	// *
//	Stack():INIT_SIZE(100),EXPAND_SIZE(10),capacity(INIT_SIZE){
//		base = new Data[INIT_SIZE];
//		top = base;
//	}
//	~Stack(){
//		if(top){
//			top = nullptr;
//		}
//		if(base){
//			delete []base;
//			base = nullptr;
//		}
//	}
//	// =============
//	
//	//top is like end in std::vector, point to the next of the last element
//	
//	bool Pop(Data &e){
//		if(top == base)
//			return false;
//		else{
//			e = *(top-1);
//			top--;
//		}
//	}
//	bool Push(Data &e){
//		if((top - base) >= capacity ){
//			// expand
//			Data *temp = new Data[capacity+EXPAND_SIZE];
//			if(!temp)
//				return false;
//			for(int i = 0; i<capacity; i++){
//				temp[i] = base[i];
//			}
//			base = temp;
//		}
//		top = base+capacity;
//		*top = e;
//		top++;
//		capacity += EXPAND_SIZE;
//		return true;
//	}
//	bool GetTop(Data &e){
//		if(top == base)
//			return false;
//		e = *(top-1);
//		return true;
//	}
//	int GetSize(){
//		return top - base;
//	}
//	bool IsEmpty(){
//		if(top == base)
//			return true;
//		else
//			return false;
//
//	}
//	bool Clear(){
//		top = base;
//		return true;
//	}
//	bool Traverse(bool (*Visit)(Data)){
//		for(int i =0; i< top - base; i++){
//			if(!Visit(base[i]))
//				return false;
//		}
//		return true;
//	}
//};