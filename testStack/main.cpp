#include "MyStack.h"
#include<iostream>
using namespace std;
int factorial( int val ){
	if( val > 1 ){
		return factorial( val - 1 ) * val;
	}
	return 1;
}
bool t(int &e){
	cout<<e<<" ";
	return true;
}
int factorial_stack( int val ){
	int result = val;
	Stack<int> mStack;
	while( result > 1 ){
		int temp = result;
		if(!mStack.Push(temp))
			throw("bad push");
		result--;
	}
	Stack<int> s2 = mStack;
	s2.Traverse(t);
	Stack<int> s3(s2);
	s3.Traverse(t);
	while(!mStack.IsEmpty()){
		int temp;
		if(!mStack.Pop(temp))
			throw("bad pop");
		result = result*temp;
	}
	return result;
}

int factorial_stack_C( int val ){
	int result = val;
	Stack_C<int> mStack;
	while( result > 1 ){
		int temp = result;
		if(!mStack.Push(temp))
			throw("bad push");
		result--;
	}
	Stack_C<int> s2 = mStack;
	s2.Traverse(t);
	Stack_C<int> s3(s2);
	s3.Traverse(t);
	while(!mStack.IsEmpty()){
		int temp;
		if(!mStack.Pop(temp))
			throw("bad pop");
		result = result*temp;
	}
	return result;
}

int factorial_stack_LL( int val ){
	int result = val;
	Stack_LL<int> mStack;
	while( result > 1 ){
		int temp = result;
		if(!mStack.Push(temp))
			throw("bad push");
		result--;
	}
	Stack_LL<int> s2 = mStack;
	s2.Traverse(t);
	Stack_LL<int> s3(mStack);
	s3.Traverse(t);
	while(!mStack.IsEmpty()){
		int temp;
		if(!mStack.Pop(temp))
			throw("bad pop");
		result = result*temp;
	}
	return result;
}
int main(){
	int n;
	cin>>n;
	cout<<"\n"<<factorial(n)<<endl;
	cout<<"\n"<<factorial_stack(n)<<endl;
	cout<<"\n"<<factorial_stack_C(n)<<endl;
	cout<<"\n"<<factorial_stack_LL(n)<<endl;

	system("pause");
	return 0;
}